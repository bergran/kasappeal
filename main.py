# -*- coding: utf-8 -*-

from app.core.Application import ApplicationKasappeal
from yaml import load

CONFIG_FILE = 'app/config/prod_config.yaml'
DEV_CONFIG_FILE = 'app/config/dev_config.yaml'

def load_config (prod_file, dev_file):
    error = True
    # loading prod_config
    try:
        f_prod = open(prod_file, 'r')
        prod_config = load(f_prod)
        error = False
    except:
        prod_config = None

    # loading dev_config
    try:
        f_dev = open(dev_file, 'r')
        dev_config = load(f_dev)
        error = False
    except:
        dev_config = None
    return [error, prod_config, dev_config]

if __name__ == '__main__':
    # Read configs
    error, prod_config, dev_config = load_config(CONFIG_FILE, DEV_CONFIG_FILE)

    if error:
        print("""
        Config files doesnt exist, you should have almost 1 config file, at
        path: `app/config` with any of these names `prod_config.yaml` or
        `dev_config`. If you dont know about this files, please contact with
        the administrator.
        """)
    else:
        if dev_config:
            app_config = dev_config.get('app', {})
            db_config = dev_config.get('database', {})
        else:
            app_config = prod_config.get('app', {})
            db_config = prod_config.get('database', {})

        config = {
            'name': app_config.get('name', 'default app'),
            'debug': app_config.get('debug', False),
            'version': app_config.get('version', 1),
            'db_host': db_config.get('host', 'localhost'),
            'db_port': db_config.get('port', 27017)
        }
        try:
            app = ApplicationKasappeal(host = app_config.get('host', '0.0.0.0')
                , port=app_config.get('port', 8080), config=config)
            app.run_app()
        except Exception as e:
            print(type(e))
            print(e.args)
            print('Error configuration server, finished task')
