# API MICRO-BLOGGING

This project will be an api to list, create and delete post that users will
interact. The message format is going to be JSON.

### Requirements
* python:3.5.4
* Ubuntu:17.10 (It's the virtual machine that i had to development)
* Docker: 17.05.0-ce
* Mongo: 3.6

### Dev Dependencies (python libraries)

You can show all dependencies on requirements.txt

### Deploy (with docker)

If you choose this option you only have to config app and execute scripts.

Config (basic) with docker is the next:
```yaml
  app:
    name: 'kasappeal_bloggin'
    host: '0.0.0.0'
    port: 8080
    debugg: true
    version: 1
  database:
    host: '172.17.0.2'
    port: 27017
```
**Note**: database host you can get it with docker inspect {database_container_name} if you havent any container created you can copy and paste this value

#### Install

* config app. **Note**:you can see config on `app/config`
* run `./build.sh`

#### Deploy

* run `./run.sh`

### Postman

I added a little sandbox on postman, its an app to do request.

* Install Postman
* At top, click on `Import` button
* Drop or select `kasappeal_blog.postman_collection.json` file and accept
* Click on `Collections` tab
* You will see a new collection called `kasappeal_blog`
* Change ip and port from all request.

**Note**: In some request you should change values, for example on Request
delete_post or get_post.

### Deploy (without docker)

No tested

### Authentication
Authentication is going to be http basic, this means all users will be
authenticated through a header called `Authorization` and his contains is going
to be `basic Base64(user:password)`

### Request

#### HELLO WORLD

* hello world (method GET).
  * Authorization Header: no
  * URI: /

#### USER

* Create User (method POST).
  * Authorization Header: no
  * URI: /api/v1/user
  * params:
    * firstname:
      * attribute: str
    * lastname:
      * attribute: str
    * username:
      * attribute: str
    * password:
      * attribute: str
    * email:
      * attribute: str

#### POSTS
* Get posts (method GET).
  * Authorization Header: yes
  * URI: /api/v1/posts/{page_number}
  * query_string:
    * order_by:
      * attribute: str
      * choices: ['date_create', 'title']
      * default: date_create
      * optional
    * order_type:
      * attribute: str
      * choices: ['DESC', 'ASC']
      * default: desc
      * optional
    * date_start(timestamp):
      * attribute: float
      * default: 0
    * date_end(timestamp):
      * attribute: float
      * optional
      * default: today, will call time when execute, only if default
    * user:
      * attribute: str
      * optional
      * it's the username, e.g 'cordelia'
  * params:
    * page:
      * attribute: int
* get Post (method GET).
  * Authorization Header: yes
  * URI: /api/v1/post/{id}
  * Params:
    * id:
      * attribute: str
* Create post (method POST).
  * Authorization Header: yes
  * URI: /api/v1/posts
  * params:
    * title:
      * attribute: str
    * img:
      * attribute: str
    * body:
      * attribute: str
* Delete Post (method DELETE).
  * Authorization Header: yes
  * URI: /api/v1/posts/{id}
  * Params:
    * id:
      * attribute: str
