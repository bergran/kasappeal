# -*- coding: utf-8 -*-

FROM python:3.5.4-alpine

# path container
COPY . /usr/src/app
WORKDIR /usr/src/app
VOLUME /usr/src/app

# Install dependencies
RUN pip install -r requirements.txt

# Share Port
EXPOSE 8080

ENTRYPOINT ["python", "main.py"]
