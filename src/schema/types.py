# -*- coding: utf-8 -*-

import re
from datetime import datetime as date

class Types(object):

    def __init__(self, schema_validations, value):
        self.schema = schema_validations
        self.value = value

    def number(self):
        max_length = self.schema.get('max', None)
        min_length = self.schema.get('min', None)
        choices = self.schema.get('choices', None)

        is_max = self.value <= max_length if max_length else True
        is_min = self.value >= min_length if min_length else True
        is_inside_choices = self.value in choices if choices else True

        if is_max and is_min and is_inside_choices:
            return self.value
        else:
            return None

    def string(self):
        pattern = self.schema.get('pattern', '')
        max_length = self.schema.get('max', None)
        min_length = self.schema.get('min', None)
        choices = self.schema.get('choices', None)

        is_pattern = re.match(pattern, self.value) if pattern else True
        is_max = len(self.value) <= max_length if max_length else True
        is_min = len(self.value) >= min_length if min_length else True
        is_inside_choices = self.value in choices if choices else True

        if is_pattern and is_max and is_min and is_inside_choices:
            return self.value
        else:
            return None

    def datetime(self):
        max_date = self.schema.get('max', None)
        min_date = self.schema.get('min', None)

        date_format = self.schema.get('format', '%Y/%m/%d')
        value = date.strptime(self.value, date_format)

        is_max = value <= max_date if max_date else True
        is_min = value >= min_date if min_date else True
        if is_max and is_min:
            return value
        else:
            return None

    def array(self):
        return self.value

    def dictionary(self):
        return self.value

    def boolean(self):
        return self.value
