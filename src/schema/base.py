# -*- coding: utf-8 -*-

import re

from datetime import datetime
from src.schema.types import Types


class Schema(object):

    TYPES = {
        "number": int,
        "float": float,
        "str": str,
        "datetime": datetime,
        "list": list,
        "boolean": bool,
        "object": dict,
    }

    def __init__(self, schema):
        self.schema = schema
        self.payload = None

    def __call__(self, method):
        async def _wrapper(handler, *args, **kwargs):
            request = handler.request
            content_type = request.headers.get('Content-Type')
            self.payload = payload = {}
            if content_type == 'application/json':
                payload = await handler.request.json()

            elif content_type == 'application/xml':
                # TODO (ABG) implementar
                pass
            elif content_type == 'application/x-www-form-urlencoded':
                # TODO (ABG) implementar
                pass
            elif request.method == 'GET':
                payload = handler.query_string()

            if self.validate(payload):
                handler.payload = self.payload.copy()
                return await method(handler, *args, **kwargs)
            else:
                # TODO(ABG) response de filter en json
                return await handler.response(400, {}, {
                    'message': 'bad request'
                })

        return _wrapper

    def validate(self, payload):
        validate = True
        for key, val in self.schema.items():
            if key in payload.keys():
                type_schema = val.get('type', 'str')
                type_object = self.TYPES.get(type_schema)
                try:
                    if type_object != datetime:
                        value = type_object(payload[key])
                    else:
                        date_fmt = self.schema[key].get('format', '%Y/%m/%d')
                        value = datetime.strptime(payload[key], date_fmt)
                except TypeError:
                    return False
                except ValueError:
                    return False
                attr_validation = self.check_attributes(type_schema, val,
                                                        value)
                if attr_validation:
                    self.payload[key] = attr_validation
                else:
                    return False
            elif val.get('strict', False):
                return False
            # if not strict and key doesnt found in the payload is valid
        return validate

    def check_attributes(self, type_schema, schema, value):
        type_object = Types(schema, value)
        if type_schema in ('str', 'string'):
            return type_object.string()
        elif type_schema in ('number', 'int', 'float'):
            return type_object.number()
        elif type_schema in ('list', 'array'):
            return type_object.list()
        elif type_schema in ('object', 'dictionary'):
            return type_object.dictionary()
        elif type_schema in ('datetime', 'date'):
            return type_object.datetime()
        elif type_schema in ('boolean'):
            return type_object.boolean()
        else:
            return None
