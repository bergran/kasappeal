# -*- coding: utf-8 -*-

from bson import ObjectId
from datetime import datetime

from pymongo import ReturnDocument
from pymongo.cursor import Cursor

from src.mongo.catcher import catcher


class MongoController:
    def __init__(self, mongo_instance):
        name_collection = self.__class__.__name__.replace('Model', '')
        self.collection = mongo_instance[name_collection]

    @catcher
    async def insert_one(self, document, validation=False):
        result = self.collection.insert_one(document)
        if result:
            return self.normalize(result.inserted_id)
        else:
            return None

    @catcher
    async def get_objects(self, filter_mong=None, projection=None, **kwargs):
        collection_cursor =  self.collection.find(**{
                                                     'filter': filter_mong,
                                                     'projection': projection,
                                                     **kwargs
                                                     })
        return self.normalize(collection_cursor)
    @catcher
    async def get_one(self, filter_mong=None, projection=None, **kwargs):
        return self.normalize(self.collection.find_one(**{
                             'filter': filter_mong,
                             'projection': projection,
                             **kwargs
                             }))

    @catcher
    async def update_one(self, query, data, **kwargs):
        return self.normalize(
            self.collection.find_one_and_update(filter=query, update=data,
            return_document=ReturnDocument.AFTER)
        )

    @catcher
    async def update_many(self, query, data, **kwargs):
        return self.collection.update_many(filter=query, update=data, **kwargs)

    @staticmethod
    def normalize(value):
        if isinstance(value, list) or isinstance(value, Cursor):
            return [MongoController.normalize(field) for field in value]

        elif isinstance(value, dict):
            return {field: MongoController.normalize(value[field]) for field in value}

        elif isinstance(value, datetime):
            return value.strftime('%Y-%m-%d %H:%M:%S')

        elif isinstance(value, ObjectId):
            return str(value)

        return value
