# -*- coding: utf-8 -*-

from pymongo.errors import DuplicateKeyError

def catcher(function):
    async def wrapper(self, *args, **kwargs):
        doc = error = None
        try:
            doc = await function(self, *args, **kwargs)
        except DuplicateKeyError as exception:
            error = {
                     'code': exception.details['code'],
                     'detail': 'duplicated_register'
                     }
        return doc, error
    return wrapper
