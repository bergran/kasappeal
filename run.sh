#!/bin/bash

IMAGE=kasappeal_blog
MONGO_CONTAINER=db_kasappeal
MONGO_IMAGE=mongo:3.6.0-jessie
CONTAINER_NAME=kasappeal_blog
PORT=8000
APP_PORT=8080
APP_DIR=/usr/src/app
MACHINE_DIR=`pwd`

docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME

docker run --name $MONGO_CONTAINER \
  -dt $MONGO_IMAGE

docker run --name $CONTAINER_NAME -dt \
    -p $PORT:$APP_PORT \
    -v $MACHINE_DIR:$APP_DIR $IMAGE
