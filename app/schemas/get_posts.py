# -*- coding: utf-8 -*-

from time import time

SCHEMA = {
    'date_start': {
        'type': 'float'
    },
    'date_end': {
        'type': 'float',
        'max': time()
    },
    'order_by': {
        'type': 'str',
        'choices': ['date_create', 'title']
    },
    'order_type': {
        'type': 'str',
        'choices': ['ASC', 'DESC']
    },
    'user': {
        'type': 'str',
    }
}
