# -*- coding: utf-8 -*-

SCHEMA = {
    'firstname': {
        'type': 'str',
        'pattern': r'[A-Z][a-z]+',
        'strict': True
    },
    'lastname': {
        'type': 'str',
        'pattern': r'[A-Z][a-z]+',
        'strict': True
    },
    'username': {
        'type': 'str',
        'pattern': r'[\w]+',
        'strict': True
    },
    'email': {
        'type': 'str',
        'pattern': r"[\w!#$%&'*+-\/=?^_`{|}~;]+@[a-z]+\.[a-z]{3,4}",
        'strict': True
    },
    'password': {
        'type': 'str',
        'min': 1,
        'strict': True
    }
}
