# -*- coding: utf: 8 -*-

SCHEMA = {
    'title': {
        'type': 'str',
        'min': 1,
        'strict': True
    },
    'img': {
        'type': 'str',
        'pattern': r'https?://([w]{3}\.)?[\w]+\.[\w]+[\/\w\.=&\?]+',
        'strict': True
    },
    'body': {
        'type': 'str',
        'max': 800,
        'strict': True
    }
}
