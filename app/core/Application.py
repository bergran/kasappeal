# -*- coding: utf-8 -*-

from aiohttp.web import Application
from aiohttp.web import run_app
from pymongo import MongoClient
from app.controllers.hello_world import HelloWorldHandler
from app.controllers.user import UserHandler
from app.controllers.posts import PostHandler
from app.controllers.listposts import ListPosts

from pymongo import ASCENDING

class ApplicationKasappeal(Application):
    """
        ApplicationBase

        params:
            - host (string): app host
            - port (number): app port
            - config (dict): app config
            - mongo (MongoClient object): app database
    """
    def __init__(self, host='0.0.0.0', port=8080, config=None):
        super().__init__(debug=config['debug'])
        self.host = host
        self.port = port
        self.config = config
        self.mongo = None

    def create_path(self, path):
        prefix = '/api/v{}'.format(self.config.get('version'))
        return '{}/{}'.format(prefix, path)

    def _add_routes(self):
        self.router.add_route(method='GET', path='/',
                              handler=HelloWorldHandler, name='hello_world')
        self.router.add_route(method='POST',
                              path=self.create_path('user'),
                              handler=UserHandler, name='create_user')
        self.router.add_route(method='POST', path=self.create_path('posts'),
                              handler=PostHandler, name='posts')
        resource_post = self.router.add_resource(self.create_path('post/{id}'))
        resource_post.add_route(method='DELETE', handler=PostHandler)
        resource_post.add_route(method='GET', handler=PostHandler)
        self.router.add_route(method='GET',
                              path=self.create_path('posts/{page_number}'),
                              handler=ListPosts)

    def run_app(self, **kwargs):
        """
            Method that will connect with mongo server and then will run api
            server

            kwargs: params to run_app
        """
        # Run db
        self.mongo = MongoClient(self.config.get('db_host', 'localhost'),
                                 self.config.get('db_port', 27017)
                                )
        # Know if db is connected
        self.mongo.server_info()
        self.database = self.mongo[self.config.get('name', 'app_default')]

        # create index
        self.database['User'].create_index(
            [('username', ASCENDING)], unique = True
        )

        # Adding route
        self._add_routes()
        run_app(self, host=self.host, port=self.port, **kwargs)

    def __del__(self):
        # close db session
        self.mongo.close()
