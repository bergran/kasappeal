# -*- coding: utf-8 -*-

from app.controllers.base import BaseView
from app.models.user import UserModel
from app.schemas.create_user import SCHEMA

from src.schema.base import Schema


class UserHandler(BaseView):
    @Schema(SCHEMA)
    async def post(self):
        try:
            # creating user
            doc, error = await UserModel(self.mongo).create_user(**self.payload)
            if error and error['detail'] == 'duplicated_register':
                # user exist
                return await self.response(200, {
                    'message': 'user_exist'
                }, {})
            elif error:
                # internal error
                return await self.response(500, {}, {
                    'message': error['detail']
                })
            else:
                # user created
                payload = self.payload.copy()
                del payload['password']
                return await self.response(200, {
                    'id': doc,
                    **payload,
                    'message': 'successfully action'
                }, {})
        except Exception as e:
            print(e.args)
            return await self.response(500, {}, {
                'error': 'internal_error'
            })
