# -*- coding: utf-8 -*-

from app.controllers.base import BaseView

class HelloWorldHandler(BaseView):
    async def get (self):
        return await self.response(200, {
            'hand_shake': 'Hello world',
            'message': 'It is working'
        })
