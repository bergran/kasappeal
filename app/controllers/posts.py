# -*- coding: utf-8 -*-

from time import time

from app.controllers.base import BaseView
from app.models.posts import PostsModel
from app.schemas.create_post import SCHEMA

from src.schema.base import Schema

class PostHandler(BaseView):
    async def get(self):
        auth = await self.is_auth()
        if auth['is_correct']:
            # get post
            post_id = self.request.match_info['id']
            doc, err = await PostsModel(self.mongo).get_post(post_id)
            if err:
                return await self.response(500, {}, {
                    'message': err['detail']
                })
            else:
                return await self.response(200, doc, {})
        elif auth['error']:
            # auth error
            return await self.response(500, {}, {
                'message': auth['message']
            })
        else:
            # auth forbidden
            return await self.response(403, {}, {
                'message': 'user_not_authorized'
            })

    @Schema(SCHEMA)
    async def post(self):
        auth = await self.is_auth()
        if auth['is_correct']:
            # create post
            user = auth['user']
            post = {
                **self.payload,
                'id_user': user['_id'],
                'date_create': time(),
                'deleted': False
            }

            doc, err = await PostsModel(self.mongo).create_post(**post)
            if err:
                # internal error at insert post
                return await self.response(500, {}, {
                    'message': err['detail']
                })
            else:
                # response created post successfully
                return await self.response(200, post, {})
        elif auth['error']:
            # auth error
            return await self.response(500, {}, {
                'message': auth['message']
            })
        else:
            # auth forbidden
            return await self.response(403, {}, {
                'message': 'user_not_authorized'
            })

    async def delete(self):
        auth = await self.is_auth()
        if auth['is_correct']:
            # delete post
            id_user = auth['user']['_id']
            id_post = self.request.match_info['id']
            post = {
                'id_post': id_post,
                'id_user': id_user
            }
            doc, err = await PostsModel(self.mongo).remove_post(**post)
            # doc is null if didnt remove post. by security is not an error
            response = doc if doc else {}
            if err:
                # internal error at delete
                return await self.response(500, {}, {
                    'message': err['detail']
                })
            else:
                # response delete successfully
                return await self.response(200, response, {})
        elif auth['error']:
            # auth error
            return await self.response(500, {}, {
                'message': auth['message']
            })
        else:
            # auth forbidden
            return await self.response(403, {}, {
                'message': 'user_not_authorized'
            })
