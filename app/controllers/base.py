# -*- coding: utf-8 -*-

from base64 import b64decode

from aiohttp.web import View
from aiohttp.web import Response, json_response

from app.models.user import UserModel


class BaseView(View):
    def __init__(self, request, **kwargs):
        super().__init__(request, **kwargs)
        self.content_type = self.request.content_type
        self.headers = self.request.headers
        self.mongo = self.request.app.database
        self.method = self.request.method[0]

    async def response(self, code_response, response={}, error = {}):
        data = {
            'payload': response,
            'error': error
        }
        responseObject = json_response(data, status = code_response)
        await responseObject.prepare(self.request)
        return responseObject

    def query_string(self):
        return self.request.rel_url.query

    async def is_auth(self):
        # get header
        self.authorization = self.headers.get('Authorization', '')

        # check header is not empty
        if self.authorization != '':
            # decode Authorization
            auth_encode = self.authorization.split(' ')[1]
            user, password = b64decode(auth_encode).decode('utf-8').split(':')
            return await UserModel(self.mongo).is_correct(user, password)
        else:
            # empty header
            return {
                'is_correct': False,
                'error': True,
                'message': 'Authorization header doesnt exist',
                'user': {}
            }
