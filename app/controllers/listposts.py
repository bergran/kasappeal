# -*- coding: utf-8 -*-

from time import time

from app.controllers.base import BaseView
from app.models.posts import PostsModel
from app.models.user import UserModel
from app.schemas.get_posts import SCHEMA

from src.schema.base import Schema


class ListPosts(BaseView):
    @Schema(SCHEMA)
    async def get(self):
        auth = await self.is_auth()
        if auth['is_correct']:
            # get list
            # search user
            page_param = int(self.request.match_info['page_number'])
            page = page_param if page_param > 0 else 1
            query = {
                **self.payload,
                'pagination': page
            }
            if 'user' in self.payload.keys():
                # if user exist we will get his _id
                user = self.payload['user']
                doc_user, err_user = await UserModel(self.mongo).get_user(user)
                if err_user:
                    # internal error at looking for user
                    return await self.response(500, {}, {
                        'message': err['detail']
                    })
                elif doc_user:
                    # user exist, continue with request
                    query['user_id'] = doc_user['_id']
                    del query['user']
                else:
                    # user doesnt exist
                    return await self.response(404, {}, {
                        'message': 'user_doesnt_found'
                    })
            # search data
            doc, err = await PostsModel(self.mongo).get_posts(**query)
            if err:
                # internal error at looking for posts
                return await self.response(500, {}, {
                    'message': err['detail']
                })
            elif doc:
                # exist posts
                return await self.response(200, {
                    'posts': doc,
                    'page': page,
                    'items': 10
                }, {})
            else:
                # doesnt exist posts
                return await self.response(404, {
                    'posts': []
                }, {})
            return await self.response(200, {}, {})
        elif auth['error']:
            # auth error
            return await self.response(500, {}, {
                'message': auth['message']
            })
        else:
            # auth forbidden
            return await self.response(403, {}, {
                'message': 'user_not_authorized'
            })
