# -*- coding: utf-8 -*-
from src.mongo.base import MongoController
from time import time
from hashlib import sha512

class UserModel(MongoController):
    async def create_user(self,
                          firstname,
                          lastname,
                          username,
                          email,
                          password,
                          **kwargs
                         ):
        # create a user with encrypted password, salt is the time when register
        date_create = time()
        return await self.insert_one({
            'firstname': firstname,
            'lastname': lastname,
            'username': username,
            'email': email,
            'password': self.cipher_pass(date_create, password).hexdigest(),
            'date_create': date_create
        })

    async def is_correct(self, username, password):
        # looking for username
        doc, err = await self.get_one({'username': username})
        if err:
            # internal error with db
            return {
                'is_correct': False,
                'message': err['detail'],
                'user': {},
                'error': True
            }
        elif doc:
            # if no error then we check if user is the same or is empty
            is_correct = False
            username_doc = doc['username']
            user = {}
            if username == username_doc:
                # if not empty then we check if password is correct
                cipher_pass = self.cipher_pass(doc['date_create'],
                                               password).hexdigest()
                is_correct = cipher_pass == doc['password']
                user = doc
            return {
                'is_correct': is_correct,
                'user': user,
                'message': '',
                'error': False
            }
        else:
            # user doesnt exist
            return {
                'is_correct': False,
                'user': {},
                'message': 'user_does_not_exist',
                'error': False
            }

    async def get_user(self, username):
        # get a single user
        return await self.get_one({'username': username})

    def cipher_pass(self, date_create, password):
        # method to cipher password and return natural object
        date_create_str = '{}'.format(date_create)
        salt = bytes('{}{}'.format(date_create_str, password), 'utf-8')
        return sha512(salt)
