# -*- coding: utf-8 -*-

from time import time

from bson import ObjectId
from pymongo import ASCENDING, DESCENDING

from src.mongo.base import MongoController

class PostsModel(MongoController):
    async def get_posts(
                         self,
                         order_by='date_create',
                         order_type='DESC',
                         pagination=1,
                         items=10,
                         date_start=0,
                         date_end=time,
                         user_id=None
                        ):
        # creating query
        query = {
            'date_create': {
                '$gte': date_start,
                '$lte': date_end if type(date_end) == 'float' else date_end()
            }
        }
        if user_id:
            # if user exist then we will add to query
            query['id_user'] = ObjectId(user_id)

        # normalize sort_type to mongo understand
        sort_type = ASCENDING if order_type == 'ASC' else DESCENDING
        sort = [(
            order_by, sort_type
        )]

        # create a pagination system
        skip = (pagination - 1) * items
        return await self.get_objects(filter=query, sort=sort, limit=items,
                                      skip=skip)

    async def get_post(self, idPost):
        # get for single post
        return await self.get_one(filter={
            '_id': ObjectId(idPost)
        })

    async def create_post(
                           self,
                           title,
                           img,
                           body,
                           id_user,
                           date_create,
                           deleted
                          ):
        # create a single post
        return await self.insert_one({
            'title': title,
            'img': img,
            'body': body,
            'id_user': ObjectId(id_user),
            'date_create': date_create,
            'deleted': deleted
        })

    async def remove_post(self, id_post, id_user):
        # remove a post, logic remove
        return await self.update_one(query={
            '_id': ObjectId(id_post),
            'id_user': ObjectId(id_user)
        }, data={
            '$set': {
                'deleted': True
            }
        })
